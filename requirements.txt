APScheduler==3.5.1
cached-property==1.4.3
Flask==1.0.2
Flask-Migrate==2.2.0
Flask-SQLAlchemy==2.3.2
gunicorn==19.8.1
parsel==1.4.0
requests==2.19.0
