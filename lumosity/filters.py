from os.path import join, getmtime

from lumosity.app import app


@app.template_filter('autoversion')
def autoversion_filter(filename):
    try:
        timestamp = str(getmtime(join('lumosity/', filename[1:])))
    except OSError:
        return filename

    new_filename = '{}?v={}'.format(filename, timestamp)
    return new_filename
