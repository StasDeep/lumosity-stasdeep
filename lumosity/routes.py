from flask import request, session, render_template, redirect, url_for, g, jsonify

import lumosity.service as service
from lumosity.app import app
from lumosity.helpers import is_mobile


@app.route('/', methods=('GET', 'POST', 'PUT', 'DELETE'))
def index():
    if request.method == 'GET':
        if session.get('user_id'):
            user = service.get_active_user(session['user_id'])

            if user is None:
                # If user does not exist or is not active (password is not correct on Lumosity),
                # then finish his session.
                del session['user_id']
                return render_template('login.html')

            g.user = user
            g.skills = service.get_skill_stats(user)
            g.last_refresh_hours, g.last_refresh_minutes = service.get_last_refresh_h_min(user)

            g.skill_param = request.args.get('skill')
            if g.skill_param:
                # If skill param is passed, try to show game stats.
                g.games = service.get_game_stats(user, g.skill_param, is_mobile(request))

                # If games are None, then skill_param is invalid and we need to render skill stats.
                if g.games is not None:
                    return render_template('stats_game.html')

            g.skill_param = ''
            return render_template('stats_skill.html')
        else:
            return render_template('login.html')

    if request.method == 'POST':
        login = request.form['user[login]']
        password = request.form['user[password]']
        user = service.authenticate(login, password)

        if user is None:
            return login_error()

        session['user_id'] = user.id
        session.permanent = True
        return redirect(url_for('index'))

    if request.method == 'DELETE':
        if session.get('user_id'):
            del session['user_id']
            return jsonify({'success': True})

        return jsonify({'success': False})


def login_error():
    g.error = True
    return render_template('login.html')
