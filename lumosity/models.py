from lumosity.app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(120))
    password = db.Column(db.String(120))
    name = db.Column(db.String(120))
    last_refresh = db.Column(db.DateTime)
    lumosity_cookie = db.Column(db.String(4000))

    skill_stats = db.relationship('SkillStat', backref='user', lazy=True)
    game_stats = db.relationship('GameStat', backref='user', lazy=True)

    def __repr__(self):
        return '<User {}>'.format(self.login)


class Skill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))

    stats = db.relationship('SkillStat', backref='skill', lazy=True)
    games = db.relationship('Game', backref='skill', lazy=True)

    def __repr__(self):
        return '<Skill {}>'.format(self.name)


class SkillStat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Integer)
    skill_id = db.Column(db.Integer, db.ForeignKey('skill.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return '<SkillStat {} of {}>'.format(self.skill.name, self.user.name)


class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    icon_url = db.Column(db.String(240))
    url = db.Column(db.String(120))
    skill_id = db.Column(db.Integer, db.ForeignKey('skill.id'), nullable=False)

    stats = db.relationship('GameStat', backref='game', lazy=True)

    def __repr__(self):
        return '<Game {} ({})>'.format(self.name, self.skill.name)


class GameStat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Integer)
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return '<GameStat {} of {}>'.format(self.game.name, self.user.name)
