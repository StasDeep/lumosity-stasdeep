import json
from threading import Thread

import requests
from cached_property import cached_property
from parsel import Selector as Sel

HEADERS = {
    'accept': 'text/html,application/xhtml+xml,'\
              'application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'max-age=0',
    'origin': 'https://www.lumosity.com',
    'referer': 'https://www.lumosity.com/authentication',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '\
                  'Chrome/65.0.3325.181 Safari/537.36',
}

LOGIN_HEADERS = {
    **HEADERS,
    'content-type': 'application/x-www-form-urlencoded',
    'upgrade-insecure-requests': '1',
}


class LumosityUser:

    def __init__(self, login=None, password=None, cookie=None):
        # Creds for logging, in case when cookies are not set or invalid.
        self.login = login
        self.password = password
        self.name = None

        # Cookies can be passed as a JSON-string, a dict or a CookieJar object.
        if isinstance(cookie, str):
            self.cookie_jar = requests.utils.cookiejar_from_dict(json.loads(cookie))
        elif isinstance(cookie, dict):
            self.cookie_jar = requests.utils.cookiejar_from_dict(cookie)
        else:
            self.cookie_jar = cookie

        # A separate session is created for each user.
        self.session = requests.Session()
        if self.cookie_jar is not None:
            # If cookies were provided, we can use it for further requests.
            self.session.cookies = self.cookie_jar
        else:
            # Otherwise, we need to authenticate with login and password.
            self.authenticate_session()

        self._skill_stats = None
        self._game_stats = None

    @cached_property
    def stats(self):
        for _ in range(2):
            # Send requests in parallel.
            t_skill = Thread(target=self.parse_skill_stats)
            t_game = Thread(target=self.parse_game_stats)

            t_skill.start()
            t_game.start()

            t_skill.join()

            # If we did not receive stats, then cookies failed and we need to authenticate.
            if self._skill_stats is None:
                print(f'Failed to authenticate with DB cookies ({self.login}).')
                self.authenticate_session()
                t_game.join()
            else:
                t_game.join()
                break

        return {
            'skill_stats': self._skill_stats,
            'game_stats': self._game_stats,
        }

    def parse_skill_stats(self):
        response = self.session.get('https://www.lumosity.com/app/v4/brain_profile',
                                    headers=HEADERS)

        # If we were redirected, then there is no need to parse further.
        if response.history:
            return

        sel = Sel(response.text)

        # Get array with skills stats.
        self._skill_stats = [{
            'name': row.css('label::text,.superheader::text').extract_first(),
            'value': int(row.css('.bar-chart-value::text').extract_first())
        } for row in sel.css('.bar-chart .row-fluid, #my-math-stat .span8 > .row-fluid')]

    def parse_game_stats(self):
        response = self.session.get(
            'https://www.lumosity.com/app/v4/insights/reports/game_lpi_rankings', headers=HEADERS)

        # If we were redirected, then there is no need to parse further.
        if response.history:
            return

        sel = Sel(response.text)

        self._game_stats = [{
            'name': row.css('.name::text').extract_first(),
            'skill': row.css('.bac::text').extract_first(),
            'value': int(row.css('.lpi::text').extract_first()),
            'icon': row.css('.insight-game-icon img::attr(src)').extract_first(),
            'url': 'https://www.lumosity.com' \
                   + row.css('.detail-cell > a::attr(href)').extract_first()
        } for row in sel.css('.ranking-row')]

    @cached_property
    def cookie(self):
        cookie_dict = requests.utils.dict_from_cookiejar(self.cookie_jar)
        return json.dumps(cookie_dict)

    def authenticate_session(self):
        # Open authentication page to get cookies and CSRF token.
        response = self.session.get('https://www.lumosity.com/authentication', headers=HEADERS)
        sel = Sel(response.text)
        token = sel.css('meta[name="csrf-token"]::attr(content)').extract_first()

        payload = {
            'activation_code': '',
            'authenticity_token': token,
            'commit': 'Log In',
            'gapi_access_token': '',
            'id_token': '',
            'persistent_login': '1',
            'redirect_uri': '',
            'screen_resolution': '',
            'user[login]': self.login,
            'user[password]': self.password,
            'utf8': '✓'
        }

        # Log in to the account.
        response = self.session.post('https://www.lumosity.com/authentication',
                                     headers=LOGIN_HEADERS, data=payload)

        if response.status_code != 200:
            raise LumosityAuthError('Cannot authenticate')

        sel = Sel(response.text)
        self.name = sel.css('.display-name::text').extract_first()

        self.cookie_jar = self.session.cookies


class LumosityAuthError(Exception):
    pass
