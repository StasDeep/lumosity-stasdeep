import atexit

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

from lumosity.service import refresh_all


scheduler = BackgroundScheduler()
scheduler.start()

scheduler.add_job(
    func=refresh_all,
    trigger=CronTrigger(minute='*/5'),
    id='refresh_all',
    name='Refresh stats of all users',
    replace_existing=True)

# Shut down the scheduler when exiting the app
atexit.register(scheduler.shutdown)
