def is_mobile(request):
    mobiles = ['iphone', 'android']
    return any(mobile in request.headers.get('User-Agent').lower() for mobile in mobiles)
