from os.path import join, dirname, abspath

from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yX HHs!jd2mN]acX/,?RT'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + join(abspath(dirname(__file__)), 'db.sqlite')

db = SQLAlchemy(app)
migrate = Migrate(app, db, directory=join(dirname(__file__), 'migrations'))


import lumosity.filters
import lumosity.models
import lumosity.routes
import lumosity.scheduler
