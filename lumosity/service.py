from datetime import datetime, timedelta
from time import sleep

from sqlalchemy import func

from lumosity.app import db
from lumosity.lumosity_user import LumosityUser, LumosityAuthError
from lumosity.models import User, Skill, SkillStat, Game, GameStat


def authenticate(login, password):
    user = User.query.filter(User.login == login).first()

    if user is None:
        # If user is not created, need to create new one with provided credentials.
        try:
            new_user = create_user(login, password)
        except LumosityAuthError:
            return None

        return new_user
    else:
        if user.password == password:
            # NOTE: we do not check if password is still correct in Lumosity DB.
            # Periodic task should automatically reset password if it's invalid.
            return user
        else:
            # If password differs from local DB, then it was probably changed.
            # Need to check if this unknown password is correct in Lumosity DB.
            if is_correct_password(login, password):
                # Need to save correct password in local DB.
                change_password(user, password)
                return user
            else:
                return None


def change_password(user, password):
    user.password = password
    db.session.commit()


def create_user(login, password):
    lum_user = LumosityUser(login, password)

    user = User(
        login=login,
        password=password,
        name=lum_user.name,
        last_refresh=datetime.now(),
        lumosity_cookie=lum_user.cookie
    )
    db.session.add(user)
    db.session.commit()

    refresh_stats(user, lum_user)

    return user


def get_active_user(user_id):
    user = User.query.get(user_id)

    if user and user.password is not None:
        return user

    return None


def get_last_refresh_h_min(user):
    seconds = round((datetime.now() - user.last_refresh).total_seconds())
    hours, seconds = divmod(seconds, 3600)
    minutes = seconds // 60
    return hours, minutes


def get_game_stats(user, skill_name, is_mobile=False):
    skill = Skill.query.filter(func.lower(Skill.name) == func.lower(skill_name)).first()

    if skill is None:
        return None

    game_stats = [{
        'name': game.name,
        'icon_url': game.icon_url,
        'url': 'lumosity://games?key=' + game.name.replace(' ', '+') if is_mobile else game.url,
        'stats': [{
            'name': stat.user.name,
            'value': stat.value,
            'pct': stat.value / 20,
            'own': stat.user == user,
        } for stat in game.stats]
    } for game in skill.games]

    for game_stat in game_stats:
        game_stat['stats'].sort(key=lambda x: x['value'], reverse=True)

    return game_stats


def get_skill_stats(user):
    skill_stats = [{
        'name': skill.name,
        'has_games': bool(skill.games),
        'stats': [{
            'name': stat.user.name,
            'value': stat.value,
            'pct': stat.value / 20,
            'own': stat.user == user,
        } for stat in skill.stats]
    } for skill in Skill.query.all()]

    for skill_stat in skill_stats:
        skill_stat['stats'].sort(key=lambda x: x['value'], reverse=True)

    return skill_stats


def is_correct_password(login, password):
    try:
        LumosityUser(login, password)
    except LumosityAuthError:
        return False

    return True


def refresh_all():
    for user in User.query.all():
        try:
            refresh_stats(user)
        except LumosityAuthError:
            reset_password(user)
            print(f'Reset password for: {user}')

        # To avoid too intense requests.
        sleep(10)


def refresh_stats(user=None, lum_user=None):
    if lum_user is None:
        lum_user = LumosityUser(user.login, user.password, user.lumosity_cookie)

    skills_available = Skill.query.all()
    user_skill_stats = SkillStat.query.filter(SkillStat.user == user).all()

    for skill_stat in lum_user.stats['skill_stats']:
        skill = next((s for s in skills_available if s.name == skill_stat['name']), None)
        if skill is None:
            skill = Skill(name=skill_stat['name'])
            db.session.add(skill)

        user_skill_stat = next((ss for ss in user_skill_stats if ss.skill == skill), None)
        if user_skill_stat is None:
            user_skill_stat = SkillStat(skill=skill, user=user, value=skill_stat['value'])
            db.session.add(user_skill_stat)
        else:
            user_skill_stat.value = skill_stat['value']

    games_available = Game.query.all()
    user_game_stats = GameStat.query.filter(GameStat.user == user).all()

    for game_stat in lum_user.stats['game_stats']:
        game = next((g for g in games_available if g.name == game_stat['name']), None)
        if game is None:
            skill = Skill.query.filter(Skill.name == game_stat['skill']).first()
            game = Game(
                name=game_stat['name'],
                skill=skill,
                icon_url=game_stat['icon'],
                url=game_stat['url']
            )
        else:
            # Just to make sure we have the latest icon.
            game.icon_url = game_stat['icon']

        user_game_stat = next((gs for gs in user_game_stats if gs.game == game), None)
        if user_game_stat is None:
            user_game_stat = GameStat(game=game, user=user, value=game_stat['value'])
            db.session.add(user_game_stat)
        else:
            user_game_stat.value = game_stat['value']

    user.last_refresh = datetime.now()
    db.session.commit()

    return True


def reset_password(user):
    user.password = None
    user.lumosity_cookie = None
    db.session.commit()
